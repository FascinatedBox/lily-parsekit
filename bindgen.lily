import parsekit
import (BaseSym,
        ClassSym,
        ContainerSym,
        DefineSym,
        EnumSym,
        ForeignClassSym,
        DefineScope,
        ModuleSym,
        Parameter,
        TypeSym,
        VarSym,
        VariantSym,
        base_as_container) symbols

#[
bindgen
=======

This tool reads in special `/** ... */` comments in a `.c` source to generate
bindings. Those bindings consist of helper macros and information to tell the
interpreter what your module is exporting.

The format of the comments is documented in README_bindgen.md
]#

# This tool works by generating output in a series of passes.
# The output is sent here.
var gen_output: List[String] = []

# This is the current package name
var gen_pkg_name = ""

# Helper functions that will come in handy later.

define string_repeat(what: String, count: Integer): String
{
    var out = ""

    for i in 0...count - 1: {
        out = out ++ what
    }

    return out
}

define oct(num: Integer): String
{
    var out = ""

    while num: {
        var digit = num % 8
        out = digit.to_s() ++ out
        num = num / 8
    }

    return "0" ++ out
}

# Docgen's version of this function provides padding so the output is nicer.
# Bindgen's version squishes out unnecessary spaces to save, well, space. :)

define render_type(source: TypeSym): String
{
    var con = base_as_container(source.base).unwrap()
    var out = con.name
    var args = source.children.map(render_type)

    if con.is_function(): {
        var func_out = args[0]
        var inputs = args.slice(1, args.size()).join(", ")

        if func_out != "Unit": {
            func_out = "=>" ++ func_out
        else:
            func_out = ""
        }

        out = out ++ "(" ++ inputs ++ func_out ++ ")"
    elif con.is_optional():
        out = "*" ++ args[0]
    elif con.is_vararg():
        out = args[0] ++ "..."
    elif args.size():
        out = out ++ "[" ++ args.join(",") ++ "]"
    }

    return out
}

define render_parameters(source: List[Parameter]): String
{
    var out: List[String] = []
    var result_type_str = render_type(source[0].type)

    for i in 1...source.size() - 1: {
        var arg = source[i]
        var out_str = render_type(arg.type)

        if arg.keyword: {
            out_str = arg.keyword ++ " " ++ out_str
        }

        out.push(out_str)
    }

    var out_text = out.join(",")

    if out_text: {
        out_text = "(" ++ out_text ++ ")"
    }

    # The space keeps the result's type from being seen as a keyarg.
    # Ex: `x: String (x, :, String)` versus x:String `(x, :String)`.
    if result_type_str != "Unit": {
        out_text = out_text ++ ": " ++ result_type_str
    }

    return out_text
}

#[
Here are the different stages. These are organized as follows:

* Passes start with `start_<passname>`

* Helper functions are `on_<passname>_<groupname>`.

* Each stage begins with a block explaining what it does.

Three stages (offset, dyna, and loader) must walk the objects in the same order.
If they aren't, dyna table indexes are unlikely to properly map over to loader
indexes. Since this is C, that will cause the interpreter to crash.

toplevel:
    classes:
        functions
        properties

    enums:
        functions
        variants

    functions (toplevel)
    vars (toplevel)
]#

#[
Setup stage
    Foreign classes:
        Generate create wrapper struct and typedef
        Generate ARG_ macro to fetch and cast an argument.
        Generate INIT_ macro to create a new instance.

    Non-builtin classes:
        Generate ID_ macro to get runtime class id.
]#

var setup_index = 0
var variant_index = 1

define on_setup_variants(variant: VariantSym)
{
    var variant_name = variant.name
    var num_args = variant.parameters.size()
    var init_string = ""

    if num_args == 0: {
        init_string = """\
            #define PUSH_{0}(state)\\\n\
            lily_push_empty_variant(state, \
                                    lily_cid_at(state, {1}) + {2})\
        """.format(variant_name,
                   setup_index,
                   variant_index)
    else:
        init_string = """\
            #define INIT_{0}(state)\\\n\
            lily_push_variant(state, \
                              (lily_cid_at(state, {1}) + {2}), \
                              {3})\
        """.format(variant_name,
                   setup_index,
                   variant_index,
                   num_args)
    }

    gen_output.push(init_string)
    variant_index += 1
}

define on_setup_properties(source: ClassSym)
{
    var member_list = source.properties
    var source_name = source.name

    for i in 0...member_list.size() - 1: {
        var member = member_list[i]
        var member_name = member.name
        var getter_string = """\
            #define GET_{0}__{1}(c_) \\\n\
            lily_con_get(c_, {2})\
        """.format(source_name,
                   member_name,
                   i)

        var setter_string = """\
            #define SET_{0}__{1}(c_, v_) \\\n\
            lily_con_set(c_, {2}, v_)\
        """.format(source_name,
                   member_name,
                   i)

        var setfs_string = """\
            #define SETFS_{0}__{1}(state, c_) \\\n\
            lily_con_set_from_stack(state, c_, {2})\
        """.format(source_name,
                   member_name,
                   i)

        gen_output.push(getter_string)
        gen_output.push(setter_string)
        gen_output.push(setfs_string)
    }
}

define on_setup_containers(c: ContainerSym)
{
    var cls_name = c.name

    match c: {
        case EnumSym(e):
            e.variants.each(on_setup_variants)
            variant_index = 1

            gen_output.push("")
            setup_index += 1
            return
        else:
    }

    var full_name = "lily_{0}_{1}".format(gen_pkg_name, c.name)

    match c: {
        case ForeignClassSym(fcs):
            # The engine puts LILY_FOREIGN_HEADER at the top so this tool doesn't
            # have to do it.
            gen_output.push("typedef struct " ++ full_name ++ "_ {")
            fcs.layout.each(|l| gen_output.push("    " ++ l) )

            gen_output.push("} " ++ full_name ++ ";")

            gen_output.push(
            ("#define ARG_{0}(state, index) \\\n" ++
             "({1} *)lily_arg_generic(state, index)")
            .format(cls_name, full_name))

            gen_output.push("""\
                #define AS_{0}(v_)\\\n\
                (({1} *)(lily_as_generic(v_)))\
            """.format(cls_name,
                       full_name))
        case ClassSym(cs):
            on_setup_properties(cs)
        else:
    }

    gen_output.push(
    "#define ID_{0}(state) lily_cid_at(state, {1})"
    .format(cls_name, setup_index))

    match c: {
        case ForeignClassSym(fcs):
            gen_output.push(
            ("#define INIT_{0}(state)\\\n" ++
             "({1} *) lily_push_foreign(state, " ++
             "ID_{0}(state), " ++
             "(lily_destroy_func)destroy_{0}, " ++
             "sizeof({1}))")
            .format(cls_name, full_name))
        case ClassSym(cs):
            var member_count = cs.properties.size()
            var init_string = """\
                #define SUPER_{0}(state)\\\n\
                lily_push_super(state, ID_{0}(state), {1})\
            """.format(cls_name,
                       member_count)

            gen_output.push(init_string)
        else:
    }

    gen_output.push("")
    setup_index += 1
}

define start_setup(module: ModuleSym)
{
    var upper_name = module.name.upper()
    var fixed_name = "LILY_" ++ upper_name ++ "_BINDINGS_H"

    gen_output.push("""#ifndef {0}\n\
                       #define {0}\n\
                       /* Generated by lily-bindgen, do not edit. */\n\
                       """.format(fixed_name))

    gen_output.push("""#if defined(_WIN32) && \
                           !defined(LILY_NO_EXPORT)\n\
                       #define LILY_{0}_EXPORT __declspec(dllexport)\n\
                       #else\n\
                       #define LILY_{0}_EXPORT\n\
                       #endif\n\
                       """.format(upper_name))

    if module.name != "builtin": {
        module.containers.each(on_setup_containers)
    }
}

#[
Dyna stage
    This generates the dynaload table that the interpreter reads.

    Classes and enums add themselves to dyna_class_names, which is later
    written to the top of the table. The interpreter watches out for the usage
    of those names, and builds a class id (cid) table accordingly. This data is
    used at runtime by the ID_* macro to get the class id.
]#

var dyna_class_names: List[String] = []

define dyna_letter_for_container(c: ContainerSym): String
{
    var dyna_letter = ""

    match c: {
        case ForeignClassSym(fcs):
            dyna_letter = "C\\"
        case ClassSym(cs):
            dyna_letter = "N\\"
        case EnumSym(es):
            dyna_letter = "E\\"
        else:
    }

    return dyna_letter
}

define dyna_letter_for_function(scope: DefineScope): String
{
    var dyna_letter = ""

    match scope: {
        case DefineScope.Toplevel:
            dyna_letter = "F"
        else:
            dyna_letter = "m"
    }

    return dyna_letter
}

define dyna_letter_for_var(scope: DefineScope): String
{
    var dyna_letter = ""

    match scope: {
        case DefineScope.Public:
            dyna_letter = "3"
        case DefineScope.Protected:
            dyna_letter = "2"
        case DefineScope.Private:
            dyna_letter = "1"
        case DefineScope.Toplevel:
            dyna_letter = "R"
    }

    return dyna_letter
}

define padding_for_container(c: ContainerSym): Integer
{
    var result = 0

    match c: {
        case EnumSym(es):
            if es.is_scoped(): {
                result = es.variants.size()
            }
        case ClassSym(cs):
            result = cs.properties.size()
        else:
    }

    return result
}

define parent_for_container(c: ContainerSym): Option[String]
{
    var result: Option[String] = None

    match c: {
        case ClassSym(cs):
            result = cs.parent
        case ForeignClassSym(fcs):
            result = fcs.parent
        else:
    }    

    return result
}

define on_dyna_functions(d: DefineSym)
{
    var dyna_letter = dyna_letter_for_function(d.scope)
    var generics = d.generics.join(",")

    if generics: {
        generics = "[" ++ generics ++ "]"
    }

    var proto = render_parameters(d.parameters)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
                ++ d.name ++ "\\0"
                ++ generics
                ++ proto
                ++ "\"")
}

define on_dyna_vars(v: VarSym)
{
    var dyna_letter = dyna_letter_for_var(v.scope)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ v.name ++ "\\0"
              ++ render_type(v.type)
              ++ "\"")
}

define on_dyna_methods(d: DefineSym)
{
    var dyna_letter = dyna_letter_for_function(d.scope)
    var generics = d.generics.join(",")

    if generics: {
        generics = "[" ++ generics ++ "]"
    }

    var proto = render_parameters(d.parameters)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
                ++ d.name ++ "\\0"
                ++ generics
                ++ proto
                ++ "\"")
}

define on_dyna_properties(v: VarSym)
{
    var dyna_letter = dyna_letter_for_var(v.scope)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ v.name ++ "\\0"
              ++ render_type(v.type)
              ++ "\"")
}

define on_dyna_variants(v: VariantSym)
{
    var dyna_letter = "V"
    var type_str = ""
    var out: List[String] = []
    var args = v.parameters

    for i in 0...args.size() - 1: {
        var arg = args[i]
        var out_str = render_type(arg.type)

        if arg.keyword: {
            out_str = arg.keyword ++ " " ++ out_str
        }

        out.push(out_str)
    }

    if out.size(): {
        type_str = "(" ++ out.join(",") ++ ")"
    }

    gen_output.push(
    "    ,\"" ++ dyna_letter
              ++ "\\0"
              ++ v.name
              ++ "\\0"
              ++ type_str
              ++ "\"")
}

define on_dyna_containers(c: ContainerSym)
{
    dyna_class_names.push(c.name)

    var dyna_letter = dyna_letter_for_container(c)
    var generics = c.generics.join(",")
    var parent = parent_for_container(c)
    var suffix = "\\0"
    var total_size = c.methods.size() + padding_for_container(c)

    if generics: {
        suffix = suffix ++ "[" ++ generics ++ "]"
    }

    if parent.is_some(): {
        suffix = suffix ++ "< " ++ parent.unwrap()
    }

    gen_output.push(
    ["    ,\"", dyna_letter, oct(total_size), c.name, suffix, "\""].join())

    c.methods.each(on_dyna_methods)

    match c: {
        case EnumSym(es):
            es.variants.each(on_dyna_variants)

        case ClassSym(cs):
            cs.properties.each(on_dyna_properties)

        else:
    }
}

define start_dyna(module: ModuleSym)
{
    gen_output.push("LILY_{0}_EXPORT".format(module.name.upper()))
    gen_output.push("const char *lily_" ++ gen_pkg_name ++ "_info_table[] = {")
    gen_output.push("")
    var patch_index = gen_output.size()

    module.containers.each(on_dyna_containers)
    module.definitions.each(on_dyna_functions)
    module.vars.each(on_dyna_vars)

    gen_output.push("    ,\"Z\"")
    gen_output.push("};")

    if gen_pkg_name == "builtin": {
        dyna_class_names = []
    }

    var dyna_names = dyna_class_names.join("\\0")
    var dyna_size = dyna_class_names.size() |> oct

    gen_output[patch_index-1] =
            "    \"\\{0}{1}\\0\"".format(dyna_size, dyna_names)
}

#[
Offset stage
    This stage is used by the builtin module. Offsets are used to determine
    where builtin classes start off in the table.
]#

var offset_index = 1
var offset_pairs: Hash[String, Integer] = []

define on_offset_containers(c: ContainerSym)
{
    var name = c.name

    gen_output.push("#define " ++ name ++ "_OFFSET " ++ offset_index)
    offset_pairs[name] = offset_index

    offset_index += c.methods.size() + 1

    match c: {
        case ClassSym(cs):
            offset_index += cs.properties.size()
        case EnumSym(es):
            offset_index += es.variants.size()
        else:
    }
}

define start_offsets(module: ModuleSym)
{
    if module.name == "builtin": {
        module.containers.each(on_offset_containers)
    }
}

#[
Call Table stage
    The call table holds pointers that the interpreter uses to carry out the
    action for an index given. For method indexes, it'll send back a method.
    For vars, it'll send back the var loader. Both kinds of functions have the
    same signature, so they're stored in the same table.
    This table is NULL-padded to account for the string table holding entries
    that don't need a companion (like enum and variant declarations).
]#

define on_call_table_vars(v: VarSym)
{
    var load_name = "    lily_{0}_var_{1}, \\".format(gen_pkg_name, v.name)
    gen_output.push(load_name)
}

define on_call_table_methods(c: ContainerSym, d: DefineSym)
{
    var name = d.name

    if d.is_ctor(): {
        name = "new"
    }

    var load_name = "    lily_{0}_{1}_{2}, \\"
            .format(gen_pkg_name, c.name, name)

    gen_output.push(load_name)
}

define on_call_table_functions(d: DefineSym)
{
    var load_name = "    lily_{0}__{1}, \\".format(gen_pkg_name, d.name)

    gen_output.push(load_name)
}

define on_call_table_empty_slot[A](a: A)
{
    gen_output.push("    NULL, \\")
}

define on_call_table_containers(c: ContainerSym)
{
    gen_output.push("    NULL, \\")

    c.methods.each(|d| on_call_table_methods(c, d) )

    match c: {
        case ClassSym(cs):
            cs.properties.each(on_call_table_empty_slot)
        case EnumSym(es):
            es.variants.each(on_call_table_empty_slot)
        else:
    }
}

define start_call_table(module: ModuleSym)
{
    gen_output.push("""#define LILY_DECLARE_{0}_CALL_TABLE \\\n\
                       LILY_{0}_EXPORT \\\n\
                       lily_call_entry_func lily_{1}_call_table[] = {2} \\"""
                    .format(gen_pkg_name.upper(), gen_pkg_name, "{"))

    # This accounts for the info table having a header line.
    gen_output.push("    NULL, \\")

    module.containers.each(on_call_table_containers)
    module.definitions.each(on_call_table_functions)
    module.vars.each(on_call_table_vars)

    gen_output.push("""};\n\
                       #endif""")
}

define load_source_lines(name: String): List[String]
{
    var f = File.open(name, "r")
    var lines: List[String] = []

    f.each_line(|l| l.encode().unwrap() |> lines.push )
    f.close()

    return lines
}

define verify_path(path: String): Boolean
{
    var ok = false
    var filename = path.split("\/")[-1]

    if filename.starts_with("lily_") == false: {
        print("bindgen: Error: Filename must start with 'lily_'.")
    elif filename.ends_with(".c") == false:
        print("bindgen: Error: Filename must end with '.c'.")
    else:
        ok = true
    }

    return ok
}

define get_target_path(source_path: String): String
{
    var result = source_path.slice(0, -2) ++ "_bindings.h"

    return result
}

define run_bindgen(
    source_lines: List[String],
    path: String)
{
    if verify_path(path) == false: {
        return
    }

    var driver = parsekit.CSourceDriver()

    match driver.process_lines(source_lines): {
        case Failure(f):
            print("bindgen: Processing error: {0}\n    from line {1}."
                    .format(f[0], f[1]))
        case Success(root_module):
            var starts =
                [start_setup,
                 start_dyna,
                 start_offsets,
                 start_call_table]

            gen_pkg_name = root_module.name

            for i in 0...starts.size() - 1: {
                starts[i](root_module)
            }

            var out_path = get_target_path(path)
            var f = File.open(out_path, "w")

            gen_output.each(|e| f.print(e) )

            f.close()

            print("bindgen: Generated " ++ out_path ++ ".")
    }
}

import sys

var file_name = sys.argv[1]
print("bindgen: Processing " ++ file_name ++ ".")

var source_lines = load_source_lines(file_name)

run_bindgen(source_lines, file_name)
