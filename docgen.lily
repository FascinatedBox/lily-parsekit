# Located at FascinatedBox/lily-mkdir
# Use this until a proper filesystem package exists.
import mkdir
import parsekit
import sys

import (BaseSym,
        ClassSym,
        ContainerKind,
        ContainerSym,
        DefineScope,
        DefineSym,
        EnumSym,
        ForeignClassSym,
        ModuleSym,
        Parameter,
        TypeSym,
        VarSym,
        VariantSym,
        base_as_container) symbols

#[
docgen
======

This tool uses parsekit to read the `/** ... */` blocks in the .c file that is
provided. By default, documentation is written to "doc/" as a base.

Modules will put themselves in a directory of their own name, and place their
files inside (ex: "doc/postgres/module.postgres.html" or
                  "doc/postgres/class.Conn.html"
]#

# todo: Instead of raising an error when rendering fails, have start_docgen
#       push back an error structure instead.
class RenderError(message: String) < Exception(message) {  }

class ModuleDocState(
    public var @root: ModuleSym,
    public var @base_dir: String,
    public var @style_dir: String,
    public var @prefix: String)
{
    private var @lines: List[String] = []

    private var @class_decls: List[String] = []
    private var @enum_decls: List[String] = []

    public define load_decls {
        var containers = @root.containers

        for i in 0...containers.size() - 1: {
            var c = containers[i]
            var name = c.name

            match c: {
                case ClassSym(cs):
                    @class_decls.push(name)
                case ForeignClassSym(fcs):
                    @class_decls.push(name)
                case EnumSym(es):
                    @enum_decls.push(name)
                else:
            }
        }
    }

    public define has_class(name: String): Boolean {
        for i in 0...@class_decls.size() - 1: {
            if @class_decls[i] == name: {
                return true
            }
        }

        return false
    }

    public define has_enum(name: String): Boolean {
        for i in 0...@enum_decls.size() - 1: {
            if @enum_decls[i] == name: {
                return true
            }
        }

        return false
    }

    public define find_class_obj(name: String): ContainerSym
    {
        var classes = @root.containers

        for i in 0...classes.size() - 1: {
            var c = classes[i]
            if c.name == name: {
                return c
            }
        }

        # todo: The parsekit base should verify parent lineage
        raise RuntimeError("Could not find parent class '{0}'.".format(name))
    }

    public define push(input: String) { @lines.push(input) }

    private define title_for_sym(sym: BaseSym): String {
        var title = @root.name ++ "." ++ sym.name

        match sym: {
            case ModuleSym(ms):
                title = sym.name
            else:
        }

        return title
    }

    public static define kind_name_for_sym(sym: BaseSym): String {
        var kind = ""

        match sym: {
            case ClassSym(cs):
                kind = "class"
            case ForeignClassSym(fcs):
                kind = "class"
            case EnumSym(es):
                kind = "enum"
            case ModuleSym(ms):
                kind = "module"
            else:
        }

        return kind
    }

    public define generate_for(sym: BaseSym) {
        var name = sym.name
        var title = title_for_sym(sym)
        var kind = kind_name_for_sym(sym)

        if @prefix: {
            title = @prefix ++ title
        }

        var file_name = @base_dir ++ kind ++ "." ++ name ++ ".html"

        var header =
"""<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{title}</title>
        <link href="{style_dir}docstyle.css" rel="stylesheet">
    </head>

    <body role="document">
        <section id='main'>"""
    .replace("{style_dir}", @style_dir)
    .replace("{title}", title ++ " : Lily")

        var footer =
"""        </section>
    </body>
</html>"""
        var f = File.open(file_name, "w")
        var local_lines = @lines
    
        f.print(header)
    
        for i in 0...local_lines.size() - 1: {
            f.print(local_lines[i])
        }

        f.print(footer)
        f.close()
        @lines = []
    }
}

# Docgen's version of this function provides padding so the output is nicer.
# Bindgen's version squishes out unnecessary spaces to save, well, space. :)

define render_type(source: TypeSym): String
{
    var con = base_as_container(source.base).unwrap()
    var out = con.name
    var args = source.children.map(render_type)

    if con.is_function(): {
        var func_out = args[0]
        var inputs = args.slice(1, args.size()).join(", ")

        if func_out != "Unit": {
            func_out = " => " ++ func_out
        else:
            func_out = ""
        }

        out = out ++ "(" ++ inputs ++ func_out ++ ")"
    elif con.is_optional():
        out = "*" ++ args[0]
    elif con.is_vararg():
        out = args[0] ++ "..."
    elif args.size():
        out = out ++ "[" ++ args.join(", ") ++ "]"
    }

    return out
}

define render_linked_type(state: ModuleDocState, source: TypeSym): String
{
    var con = base_as_container(source.base).unwrap()
    var out = con.name
    var children = source.children
    var args: List[String] = []

    for i in 0...children.size() - 1: {
        render_linked_type(state, children[i]) |> args.push
    }

    if state.has_class(out): {
        out = "<a href='class.{0}.html'>{0}</a>".format(con.name)
    elif state.has_enum(out):
        out = "<a href='enum.{0}.html'>{0}</a>".format(con.name)
    }

    if con.is_function(): {
        var func_out = args[0]
        var inputs = args.slice(1, args.size()).join(", ")

        if func_out != "Unit": {
            func_out = " => " ++ func_out
        else:
            func_out = ""
        }

        out = out ++ "(" ++ inputs ++ func_out ++ ")"
    elif con.is_optional():
        out = "*" ++ args[0]
    elif con.is_vararg():
        out = args[0] ++ "..."
    elif args.size():
        out = out ++ "[" ++ args.join(", ") ++ "]"
    }

    return out
}

define render_args(state: ModuleDocState, d: DefineSym): String
{
    var out: List[String] = []
    var args = d.parameters
    var start = 1

    if d.is_static() &&
       d.scope != DefineScope.Toplevel: {
        start = 2
    }

    for i in start...args.size() - 1: {
        var arg = args[i]
        var name = arg.name
        var type_string = render_linked_type(state, arg.type)

        if arg.keyword: {
            name = arg.keyword ++ " " ++ name
        }

        if type_string.starts_with("*"): {
            type_string = type_string ++ " = " ++ arg.value
        }

        out.push(name ++ ": " ++ type_string)
    }

    var out_text = out.join(", ")
    var return_type = render_linked_type(state, args[0].type)

    if out_text: {
        out_text = "(" ++ out_text ++ ")"
    }

    if return_type != "Unit": {
        out_text = out_text ++ ": " ++ return_type
    }

    return out_text
}

define render_property(p: VarSym): String
{
    var name = p.name
    var qual = name

    match p.scope: {
        case DefineScope.Public:
            qual = ""
        case DefineScope.Protected:
            qual = "protected "
        case DefineScope.Private:
            qual = "private "
        else:
    }

    var type_str = render_type(p.type)

    return "    {0}var @{1}: {2}".format(qual, name, type_str)
}

define write_properties_from(state: ModuleDocState, c: ClassSym)
{
    var is_top = true

    while 1: {
        var properties = c.properties

        for i in 0...properties.size() - 1: {
            properties[i]
            |> render_property
            |> state.push
        }

        var boxed_parent_name = c.parent

        if boxed_parent_name.is_some(): {
            if (is_top && properties.size()) ||
                is_top == false: {
                state.push("")
            }

            var raw_parent_name = boxed_parent_name.unwrap()
            var parent = state.find_class_obj(raw_parent_name)

            match parent: {
                case ClassSym(cs):
                    c = cs
                else:
                    break
            }

            is_top = false
            state.push("    # From " ++ raw_parent_name)

            continue
        else:
            break
        }
    }
}

define write_variants(state: ModuleDocState, e: EnumSym)
{
    var variant_list = e.variants
    var last = variant_list.size() - 1

    for i in 0...variant_list.size() - 1: {
        var variant = variant_list[i]
        var name = variant.name
        var arg_str = variant.parameters
                             .map(|p| p.type |> render_type )
                             .join(", ")

        if arg_str: {
            arg_str = "(" ++ arg_str ++ ")"
        }

        var out = "    {0}{1}".format(name, arg_str)

        if i != last: {
            out = out ++ ","
        }

        state.push(out)
    }
}

define html_escape(input_line: String): String
{
    var out_cache: List[String] = []
    var pending_out = ""
    var raw_line = input_line.to_bytestring()
    var should_slice = false
    var pos = 0
    var slice_start = 0
    var to_push = ""

    for pos in 0...raw_line.size() - 1: {
        var ch = raw_line[pos]
        if ch == '&': {
            should_slice = true
            to_push = "&amp;"
        elif ch == '<':
            should_slice = true
            to_push = "&lt;"
        elif ch == '>':
            should_slice = true
            to_push = "&gt;"
        elif ch == '"':
            should_slice = true
            to_push = "&quot;"
        elif ch == '\'':
            should_slice = true
            to_push = "&#39;"
        }

        if should_slice: {
            out_cache.push(input_line.slice(slice_start, pos))
            out_cache.push(to_push)
            should_slice = false
            slice_start = pos + 1
        }
    }

    input_line.slice(slice_start) |> out_cache.push
    return out_cache.join()
}

define render_one_doc_line(input_line: String): String
{
    var out_cache: List[String] = []
    var pending_out = ""
    var raw_line = input_line.to_bytestring()
    var in_backtick = false
    var should_slice = false
    var pos = 0
    var slice_start = 0
    var to_push = ""

    for pos in 0...raw_line.size() - 1: {
        var ch = raw_line[pos]
        if ch == '`': {
            if in_backtick == false: {
                should_slice = true
                to_push = "<code>"
                in_backtick = true
            else:
                should_slice = true
                to_push = "</code>"
                in_backtick = false
            }
        elif ch == '&':
            should_slice = true
            to_push = "&amp;"
        elif ch == '<':
            should_slice = true
            to_push = "&lt;"
        elif ch == '>':
            should_slice = true
            to_push = "&gt;"
        elif ch == '"':
            should_slice = true
            to_push = "&quot;"
        elif ch == '\'':
            should_slice = true
            to_push = "&#39;"
        }

        if should_slice: {
            out_cache.push(input_line.slice(slice_start, pos))
            out_cache.push(to_push)
            should_slice = false
            slice_start = pos + 1
        }
    }

    if in_backtick: {
        raise RenderError("Uneven number of quote marks in: {0}."
                .format(input_line))
    }

    input_line.slice(slice_start) |> out_cache.push
    return out_cache.join()
}

#[
This implements a very small set of markdown, plus a couple of special markups.

It works by splitting along totally blank lines and then assuming that what's
united by only a single newline is part of a whole block.

It has basic support for a bulleted list, replaces backticks with code tags,
replaces html characters, and has some special section markers.

Even though it's a small subset, it's enough for the moment. Eventually, it
should be replaced with a real markdown processor (as long as the special
sections can be kept).
]#
define render_doc(sym: BaseSym, name: String): String
{
    var input_lines = sym.doc.split("\n\n")
    var result_list: List[String] = []
    var is_fenced = false

    for i in 0...input_lines.size() - 1: {
        var input = input_lines[i]
        var out_line = ""

        if input.starts_with("```") &&
           input.ends_with("```"): {
            # Assume that fenced code blocks have ``` on lines by themselves.
            # Cut by 4 instead of 3 to omit the leading+trailing newlines.

            input = input.slice(4, -4)
            out_line = "<pre>" ++
                        html_escape(input.slice(4, -4)) ++
                       "</pre>"
            result_list.push(out_line)

            continue
        }

        out_line = render_one_doc_line(input)

        if out_line.starts_with("*"): {
            var list_lines = out_line.split("\n")

            out_line = out_line.split("\n")
                        .select(|s| s.starts_with("*") )
                        .map(|m| "<li>" ++ m.slice(1).lstrip(" ") ++ "</li>\n" )
                        .join()

            out_line = "<ul>\n" ++ out_line ++ "</ul>"
        elif out_line.starts_with("#"):
            if out_line == "# Errors": {
                # Assume there's only one of these, and that it will be followed
                # by a bulleted list of errors and what raises them.
                out_line = ("<div class='linkblock' id='errors.{0}'>" ++
                            "<a href='#errors.{0}'>Errors</a>" ++
                            "</div>")
                           .format(name)
            elif out_line == "# Examples":
                # This assumes only one 'example' section per doc block.
                # This should be followed by a fenced code block.
                # Having multiple single-line examples in the block is
                # encouraged.
                out_line = ("<div class='linkblock' id='examples.{0}'>" ++
                            "<a href='#examples.{0}'>Examples</a>" ++
                            "</div>")
                           .format(name)
            else:
                out_line = "<p>" ++ out_line ++ "</p>"
            }
        else:
            out_line = "<p>" ++ out_line ++ "</p>"
        }

        result_list.push(out_line)
    }

    return result_list.join("\n")
}

define write_children[A](state: ModuleDocState,
                         title: String,
                         child_list: List[A],
                         fn: Function(ModuleDocState, A))
{
    if child_list.size(): {
        state.push("<h2>" ++ title ++ "</h2>")
        for i in 0...child_list.size() - 1: {
            fn(state, child_list[i])
        }
    }
}

define write_var(state: ModuleDocState, v: VarSym)
{
    var name = v.name
    var type = render_linked_type(state, v.type)
    var ref_name = "var." ++ name
    var doc = render_doc(v, ref_name)

    state.push(
    "<h3 id='var.{0}'><code>var <a href='#var.{0}'>{0}</a>: {1}</code></h3>"
    .format(name, type))
    state.push(
    "<div class='doc'>" ++ doc ++ "</div>")
}

define write_function(state: ModuleDocState, d: DefineSym)
{
    var name = d.name
    var arg_string = render_args(state, d)
    var note_list: List[String] = []
    var ref_name = "method."

    match d.scope: {
        case DefineScope.Toplevel:
            ref_name = "function."
        case DefineScope.Public:
        case DefineScope.Protected:
            note_list.push("protected")
        case DefineScope.Private:
            note_list.push("private")
    }

    ref_name = ref_name ++ name

    var doc = render_doc(d, ref_name)

    if d.is_static(): {
        note_list.push("static")
    }

    if d.is_ctor(): {
        note_list[-1] = "constructor"
        name = "&lt;new&gt;"
        ref_name = "method." ++ name
    }

    var note = ""

    if note_list.size(): {
        note = " <i>" ++ note_list.join(", ") ++ "</i>"
    }

    state.push(
    "<h3 id='{0}'><code>define <a href='#{0}'>{1}</a>{2}{3}</code></h3>"
    .format(ref_name, name, arg_string, note))
    state.push(
    "<div class='doc'>" ++ doc ++ "</div>")
}

define gen_enum_file(state: ModuleDocState, e: EnumSym)
{
    var name = e.name
    var generics = e.generics.join(", ")

    if generics: {
        generics = "[" ++ generics ++ "]"
    }

    var scope_str = ""

    if e.is_scoped(): {
        scope_str = "scoped "
    }

    var doc = render_doc(e, "enum." ++ name)

    state.push("<h1>Enum " ++ name ++ "</h1>")
    state.push("<pre>" ++ scope_str ++ "enum " ++ name ++ generics ++ " {")
    write_variants(state, e)
    state.push("}</pre>")
    state.push("<div class='doc'>" ++ doc ++ "</div>")
    write_children(state, "Methods", e.methods, write_function)
}

define gen_class_file(state: ModuleDocState, c: ContainerSym)
{
    var name = c.name

    state.push("<h1>Class " ++ name ++ "</h1>")

    match c: {
        case ForeignClassSym(fcs):
            # Foreign classes have "fields" with their C fields.
            # No generics or parent though.

            state.push("<pre>(foreign) class " ++ name ++ " {")

            fcs.layout.slice(1)
                      .each(|e| state.push("    " ++ e) )

            state.push("}</pre>")
        case ClassSym(cs):
            # Native classes have "properties", even if it's empty.
            # Might have generics and a parent too.

            var boxed_parent = cs.parent
            var parent = ""

            if boxed_parent.is_some(): {
                parent = " < " ++ boxed_parent.unwrap()
            }

            var generics = cs.generics.join(", ")

            if generics: {
                generics = "[" ++ generics ++ "]"
            }

            var prefix = "class "

            if cs.properties.size() == 0 && state.root.name == "builtin": {
                prefix = "(builtin) class "
            }

            state.push("<pre>" ++ prefix ++ name ++ generics ++ parent ++ " {")
            write_properties_from(state, cs)
            state.push("}</pre>")
        else:
    }

    var doc = render_doc(c, "class." ++ name)

    state.push("<div class='doc'>" ++ doc ++ "</div>")
    write_children(state, "Methods", c.methods, write_function)
}

define write_shortdesc(state: ModuleDocState, s: BaseSym)
{
    var name = s.name
    var doc = s.doc.split("\n\n")[0]
                   |> render_one_doc_line
    var kind = ""

    match s: {
        case ForeignClassSym(fcs):
            kind = "class"
        case ClassSym(cs):
            kind = "class"
        case EnumSym(es):
            kind = "enum"
        case ModuleSym(ms):
            kind = name ++ "/module"
        else:
    }

    state.push("<tr>")
    state.push(
    "<td><a class='reflink' href='{0}.{1}.html'>{1}</a></td>"
    .format(kind, name))
    state.push("<td>" ++ doc ++ "</td>")
    state.push("</tr>")
}

define write_shortdescs(state: ModuleDocState, root: ModuleSym)
{
    var class_entries: List[BaseSym] = []
    var enum_entries: List[BaseSym] = []
    var exception_entries: List[BaseSym] = []
    var module_entries = root.modules.map(|e| var b: BaseSym = e
                                              b )

    var containers = root.containers

    for i in 0...containers.size() - 1: {
        var c = containers[i]

        match c: {
            case EnumSym(es):
                enum_entries.push(c)
            case ForeignClassSym(fcs):
                class_entries.push(c)
            case ClassSym(cs):
                var name = c.name

                if name == "Exception" || name.ends_with("Error"): {
                    exception_entries.push(c)
                else:
                    class_entries.push(c)
                }
            else:
        }
    }

    var entry_list = [class_entries, enum_entries, exception_entries, module_entries]
    var titles = ["Classes", "Enums", "Exceptions", "Modules"]

    for i in 0...entry_list.size() - 1: {
        var e = entry_list[i]

        if e.size(): {
            state.push("<h2>" ++ titles[i] ++ "</h2>")
            state.push("<table><tbody>")

            for i in 0...e.size() - 1: {
                write_shortdesc(state, e[i])
            }

            state.push("</tbody></table>")
        }
    }
}

define gen_module_file(state: ModuleDocState)
{
    var root = state.root
    var root_name = root.name
    var ref_name = "module." ++ root_name
    var module_doc = render_doc(root, ref_name)

    state.push("<h1>Module " ++ root_name ++ "</h1>")
    state.push("<div class='doc'>" ++ module_doc ++ "</div>")
    write_shortdescs(state, root)
    write_children(state, "functions", root.definitions, write_function)
    write_children(state, "vars", root.vars, write_var)

    mkdir.mkdir(state.base_dir)
    state.generate_for(root)

    var containers = root.containers

    for i in 0...containers.size() - 1: {
        var c = containers[i]

        match c: {
            case EnumSym(es):
                gen_enum_file(state, es)
            case ClassSym(cs):
                gen_class_file(state, cs)
            case ForeignClassSym(fcs):
                gen_class_file(state, fcs)
            else:
        }

        state.generate_for(c)
    }

    var modules = root.modules

    if modules.size(): {
        var new_style = state.style_dir ++ "../"
        var prefix = root_name ++ "."

        if prefix == "core.": {
            prefix = ""
        }

        for i in 0...modules.size() - 1: {
            var m = modules[i]
            var new_base = state.base_dir ++ m.name ++ "/"

            ModuleDocState(m, new_base, new_style, prefix)
            |> gen_module_file
        }
    }
}

define create_docstyle_css(asset_dir: String, doc_dir: String)
{
    var asset_in = File.open(asset_dir ++ "assets/docstyle.css", "r")
    var asset_out = File.open(doc_dir ++ "docstyle.css", "w")

    asset_in.each_line(|l| asset_out.write(l) )
    asset_in.close()
    asset_out.close()
}

define start_docgen(root: ModuleSym, base_dir: String, asset_dir: String)
{
    var module_dir = base_dir ++ root.name ++ "/"
    var state = ModuleDocState(root, module_dir, "../", "")

    state.load_decls()
    mkdir.mkdir(base_dir)
    gen_module_file(state)
}

var file_name = sys.argv[1]
var base_dir = sys.argv.get(2).unwrap_or("doc/")
var p = parsekit.CSourceDriver()

match p.process_file(file_name): {
    case Failure(f):
        print("docgen: Processing error: {0}\n    from line {1}."
                .format(f[0], f[1]))
    case Success(root_module):
        if base_dir && base_dir.ends_with("/") == false: {
            base_dir = base_dir ++ "/"
        }

        var invoke_split_dir = sys.argv[0].split("/")

        invoke_split_dir[-1] = ""

        var asset_dir = invoke_split_dir.join("/")

        start_docgen(root_module, base_dir, asset_dir)
        create_docstyle_css(asset_dir, base_dir)
}
