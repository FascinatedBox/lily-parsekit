#[
parsekit
========

The base of parsekit is the `EngineCore`, which is able to parse Lily snippets.
Those snippets are transformed into Json-like objects (see JsValue defined
below).

This file also includes `CSourceDriver`, which reads special `/** ... */`
comments in a .c file. Those are then fed incrementally to `EngineCore`, with
the driver returning the engine's tree root.
]#

import (BaseSym,
        ClassSym,
        ContainerSym,
        ContainerKind,
        DefineSym,
        EnumSym,
        ForeignClassSym,
        DefineScope,
        ModuleSym,
        Parameter,
        TypeSym,
        VarSym,
        VariantSym) symbols

class ParseError(message: String) < Exception(message) {  }
class BlockError(public var @line: Integer, message: String) < Exception(message) {  }

# The first order of business is creating a lexer that understands enough of
# Lily's tokenizing to be able to scan definitions.

scoped enum Token {
    Arrow,
    Colon,
    Comma,
    Digit,
    DollarSign,
    Dot,
    DoubleQuote,
    EndOfFile,
    Equal,
    Keyarg,
    LeftBracket,
    LeftCurly,
    LeftParenth,
    Less,
    Minus,
    Multiply,
    Newline,
    PropWord,
    RightBracket,
    RightCurly,
    RightParenth,
    Semicolon,
    ThreeDots,
    Word,
    Invalid
}

var char_class = List.repeat(256, Token.Invalid)

for i in 'a'.to_i()...'z'.to_i(): {
    char_class[i] = Token.Word
}

for i in 'A'.to_i()...'Z'.to_i(): {
    char_class[i] = Token.Word
}

for i in '0'.to_i()...'9'.to_i(): {
    char_class[i] = Token.Digit
}

char_class['@'] = Token.PropWord
char_class['_'] = Token.Word
char_class['['] = Token.LeftBracket
char_class[']'] = Token.RightBracket
char_class['('] = Token.LeftParenth
char_class[')'] = Token.RightParenth
char_class['{'] = Token.LeftCurly
char_class['}'] = Token.RightCurly
char_class[':'] = Token.Colon
char_class[';'] = Token.Semicolon
char_class[','] = Token.Comma
char_class['*'] = Token.Multiply
char_class['='] = Token.Equal
char_class['"'] = Token.DoubleQuote
char_class['-'] = Token.Minus
char_class['.'] = Token.Dot
char_class['<'] = Token.Less
char_class['$'] = Token.DollarSign
char_class['\r'] = Token.Newline
char_class['\n'] = Token.Newline

class EngineScanner
{
    private var @source_bytes = B""
    private var @source_pos = 0
    private var @source = ""
    private var @last_tok = Token.EndOfFile
    private var @last_str = ""
    public var @line_num = 0

    public define set_source(new_source: String, offset: *Integer = 1) {
        new_source = new_source ++ "\n"
        @source = new_source
        @source_bytes = new_source.to_bytestring()
        @source_pos = 0
        @line_num = offset
    }

    public define next {
        var result_tok = Token.EndOfFile
        var result_str = ""

        while 1: {
            var ch = @source_bytes[@source_pos]

            while ch == ' ' || ch == '\t': {
                @source_pos += 1
                ch = @source_bytes[@source_pos]
            }

            result_tok = char_class[ch]

            match result_tok: {
                case Token.Word:
                    var start = @source_pos
                    var cc = char_class[ch]

                    while cc == Token.Word || cc == Token.Digit: {
                        @source_pos += 1
                        ch = @source_bytes[@source_pos]
                        cc = char_class[ch]
                    }

                    result_str = @source.slice(start, @source_pos)
                case Token.LeftParenth:
                    @source_pos += 1
                case Token.RightParenth:
                    @source_pos += 1
                case Token.LeftBracket:
                    @source_pos += 1
                case Token.RightBracket:
                    @source_pos += 1
                case Token.LeftCurly:
                    @source_pos += 1
                case Token.RightCurly:
                    @source_pos += 1
                case Token.Colon:
                    @source_pos += 1

                    ch = @source_bytes[@source_pos]
                    var cc = char_class[ch]

                    if cc == Token.Word || cc == Token.Digit: {
                        var start = @source_pos - 1

                        while cc == Token.Word || cc == Token.Digit: {
                            @source_pos += 1
                            ch = @source_bytes[@source_pos]
                            cc = char_class[ch]
                        }

                        result_str = @source.slice(start, @source_pos)
                        result_tok = Token.Keyarg
                    }
                case Token.Less:
                    @source_pos += 1
                case Token.Semicolon:
                    @source_pos += 1
                case Token.Comma:
                    @source_pos += 1
                case Token.Multiply:
                    @source_pos += 1
                case Token.Arrow:
                    @source_pos += 1
                case Token.Dot:
                    @source_pos += 1

                    if @source_bytes[@source_pos] == '.' &&
                       @source_bytes[@source_pos + 1] == '.': {
                       @source_pos += 2
                       result_tok = Token.ThreeDots
                    }

                case Token.Equal:
                    @source_pos += 1
                    if @source_bytes[@source_pos] == '>': {
                        result_tok = Token.Arrow
                        @source_pos += 1
                    }

                case Token.Digit:
                    var start = @source_pos
                    var cc = char_class[ch]

                    while cc == Token.Digit: {
                        @source_pos += 1
                        ch = @source_bytes[@source_pos]
                        cc = char_class[ch]
                    }

                    result_str = @source.slice(start, @source_pos)
                case Token.PropWord:
                    @source_pos += 1
                    ch = @source_bytes[@source_pos]

                    var start = @source_pos
                    var cc = char_class[ch]

                    while cc == Token.Word || cc == Token.Digit: {
                        @source_pos += 1
                        ch = @source_bytes[@source_pos]
                        cc = char_class[ch]
                    }

                    result_str = @source.slice(start, @source_pos)

                    if result_str == "": {
                        result_tok = Token.Invalid
                    }
                case Token.Minus:
                    var start = @source_pos
                    @source_pos += 1

                    ch = @source_bytes[@source_pos]
                    var cc = char_class[ch]

                    if cc == Token.Digit: {
                        while cc == Token.Digit: {
                            @source_pos += 1
                            ch = @source_bytes[@source_pos]
                            cc = char_class[ch]
                        }

                        result_str = @source.slice(start, @source_pos)
                        result_tok = Token.Digit
                    }

                case Token.DoubleQuote:
                    var start = @source_pos

                    @source_pos += 1
                    ch = @source_bytes[@source_pos]

                    while ch != '"': {
                        @source_pos += 1
                        ch = @source_bytes[@source_pos]
                    }

                    @source_pos += 1
                    result_str = @source.slice(start, @source_pos)
                case Token.DollarSign:
                    ch = @source_bytes[@source_pos + 1]

                    if ch != '1': {
                        raise ParseError(
                                "Invalid '$' marker, expected $1.")
                    }

                    @source_pos += 2

                    result_str = "$1"
                    result_tok = Token.Word
                case Token.Newline:
                    if @source_pos == @source_bytes.size() - 1: {
                        result_tok = Token.EndOfFile
                        @source_pos -= 1
                    else:
                        @line_num += 1
                        @source_pos += 1
                        continue
                    }
                else:
                    # The other token kinds aren't seeded into ch_class, and
                    # therefore aren't possible to see here.
            }

            break
        }

        @last_tok = result_tok
        @last_str = result_str
    }

    public define next_tok: Token {
        next()
        return @last_tok
    }

    public define current_tok: Token {
        return @last_tok
    }

    public define current_str: String {
        return @last_str
    }

    public define current_is(expected_token: Token): Boolean {
        return @last_tok == expected_token
    }

    public define next_is(expected_token: Token): Boolean {
        next()
        return @last_tok == expected_token
    }

    public define expect(tok: Token) {
        if @last_tok != tok: {
            var got = "{0}".format(@last_tok)

            if @last_tok == Token.Word: {
                got = "'{0}'".format(@last_tok)
            }

            raise ParseError("Expected {0}, not {1}.".format(tok, got))
        }
    }

    public define expect_word(value: *String=""): String {
        if @last_tok != Token.Word ||
            (value != "" && @last_str != value): {
            raise ParseError(
                    "Expected '{0}', not '{1}.".format(value, @last_tok))
        }

        return @last_str
    }

    public define expect_any(tokens: Token...) {
        raise ParseError("Expected one of {0} but got {1})."
                .format(tokens.map(|t| "{0}".format(t) ).join(", "), @last_tok))
    }

    public define require(tok: Token): String {
        next()
        expect(tok)
        return @last_str
    }

    public define require_word(value: String): String {
        next()
        return expect_word(value)
    }

    # Returns a slice from the current position to the end.
    #
    # This returns a slice containing whatever is left in the scanner. This is
    # used primarily to get the doc blocks that occur after commands.
    #
    # This doesn't modify the scanner's position.
    public define strify_to_end: String {
        return @source.slice(@source_pos)
    }
}

define init_generic: ModuleSym {
    var m = ModuleSym("")
    var names = ["A", "B", "C", "D", "E", "Too many generics"]
    var empty: List[String] = []

    for i in 0...names.size() - 1: {
        var n = names[i]

        m.add_container(ClassSym(n, empty))
    }

    return m
}

define init_magic: ModuleSym {
    var m = ModuleSym("")
    var empty: List[String] = []

    m.add_container(ClassSym("self", empty, ContainerKind.KSimple))
    m.add_container(ClassSym("...",  empty, ContainerKind.KVararg))
    m.add_container(ClassSym("*",    empty, ContainerKind.KOptional))
    m.add_container(ClassSym("$1",   empty))

    return m
}

define init_predefined: ModuleSym {
    var m = ModuleSym("")
    var empty: List[String] = []

    m.add_container(ClassSym("Boolean",             empty))
    m.add_container(ClassSym("Byte",                empty))
    m.add_container(ClassSym("ByteString",          empty))
    m.add_container(ClassSym("Coroutine",           ["A", "B"]))
    m.add_container(ClassSym("DivisionByZeroError", empty))
    m.add_container(ClassSym("Double",              empty))
    m.add_container(ClassSym("Exception",           empty))
    m.add_container(ClassSym("File",                empty))
    m.add_container(ClassSym("Function",            empty, ContainerKind.KFunction))
    m.add_container(ClassSym("Hash",                ["A", "B"]))
    m.add_container(ClassSym("IndexError",          empty))
    m.add_container(ClassSym("Integer",             empty))
    m.add_container(ClassSym("IOError",             empty))
    m.add_container(ClassSym("KeyError",            empty))
    m.add_container(ClassSym("List",                ["A"]))
    m.add_container(ClassSym("Option",              ["A"]))
    m.add_container(ClassSym("Result",              ["A", "B"]))
    m.add_container(ClassSym("RuntimeError",        empty))
    m.add_container(ClassSym("String",              empty))
    m.add_container(ClassSym("Tuple",               empty, ContainerKind.KTuple))
    m.add_container(ClassSym("Unit",                empty))
    m.add_container(ClassSym("ValueError",          empty))

    return m
}

define find_magic_cls(m: ModuleSym, name: String): ContainerSym
{
    return m.find_container(name).unwrap()
}

class CSourceDriver {
    public var @pending_doc = ""
    private var @lines: List[String] = []
    private var @read_pos = 0
    private var @read_end_pos = 0

    public var @scanner = EngineScanner()

    private var @full_generic_names =
        ["A", "B", "C", "D", "E", "Too many generics"]

    # Definitions store a copy of generics here since they add to generics.
    private var @saved_generics: List[String] = []
    private var @visible_generics: List[String] = []

    private var @current_scope_name = ""
    private var @paths: List[String] = []

    private var @predefined_module = init_predefined()
    private var @generic_module = init_generic()
    private var @magic_module = init_magic()

    private var @function_cls = find_magic_cls(@predefined_module, "Function")
    private var @function_ty = TypeSym(@function_cls, [])

    private var @self_cls = find_magic_cls(@magic_module, "self")
    # This is a placeholder for the real self value, which is the base class
    # type plus the generics.
    private var @self_ty = @function_ty

    private var @unit_cls = find_magic_cls(@predefined_module, "Unit")
    private var @unit_ty = TypeSym(@unit_cls, [])
    private var @unit_param = Parameter("", "", @unit_ty, "")

    private var @vararg_cls = find_magic_cls(@magic_module, "...")
    private var @vararg_ty = TypeSym(@vararg_cls, [])

    private var @optional_cls = find_magic_cls(@magic_module, "*")

    private var @root_module = @predefined_module

    # This is swapped out before it's ever assigned to, but it needs a valid
    # starting value.
    private var @target_container = @optional_cls

    private define leave_scope {
        @current_scope_name = ""
        @visible_generics = []
    }

    private define get_scope_name: String {
        var name = @scanner.require(Token.Word)

        if @current_scope_name != "": {
            leave_scope()
        }

        @current_scope_name = name
        return name
    }

    private define scan_optarg_for(name: String): String {
        var expect_tok = Token.Invalid

        if name == "String": {
            expect_tok = Token.DoubleQuote
        elif name == "Integer":
            expect_tok = Token.Digit
        elif name == "Boolean":
            expect_tok = Token.Word
        }

        @scanner.require(expect_tok)

        return @scanner.current_str()
    }

    private define find_generic(name: String): Option[ContainerSym] {
        var result = @generic_module.find_container(name)
        if result.is_some(): {
            var found = false
            var g = @visible_generics
            for i in 0...g.size() - 1: {
                if g[i] == name: {
                    found = true
                    break
                }
            }

            if found == false: {
                result = None
            }
        }

        return result
    }

    public define search_for(name: String): Option[ContainerSym] {
        var result = @root_module.find_container(name)

        if result.is_none(): {
            result = @predefined_module.find_container(name)
            if result.is_none(): {
                result = @magic_module.find_container(name)
                if result.is_none(): {
                    result = find_generic(name)
                }
            }
        }

        return result
    }

    public define parse_type: TypeSym {
        @scanner.expect(Token.Word)

        var class_name = @scanner.current_str()
        var entry = search_for(class_name)

        if entry.is_none(): {
            raise ParseError("Class '{0}' does not exist.".format(class_name))
        }

        var raw_class = entry.unwrap()
        var args: List[TypeSym] = []

        if raw_class.is_simple() &&
           raw_class.generics.size() == 0: {

        elif raw_class.is_function() == false:
            var i = 0

            @scanner.require(Token.LeftBracket)
            @scanner.next()

            while 1: {
                args.push(parse_type())
                @scanner.next()
                i += 1

                match @scanner.current_tok(): {
                    case Token.Comma:
                        @scanner.next()
                        continue
                    case Token.RightBracket:
                        break
                    else:
                        @scanner.expect_any(Token.Comma, Token.LeftBracket)
                }
            }

            if i != raw_class.generic_count && raw_class.is_tuple() == false: {
                raise ParseError("Wrong number of types for {0} ({1} for {2})."
                        .format(raw_class.name, i, raw_class.generic_count))
            }
        else:
            @scanner.require(Token.LeftParenth)
            args.push(@unit_ty)

            @scanner.next()

            if @scanner.current_tok() != Token.RightParenth &&
               @scanner.current_tok() != Token.Arrow: {

                while 1: {
                    @scanner.expect(Token.Word)
                    args.push(parse_type())

                    @scanner.next()

                    if @scanner.current_tok() == Token.Comma: {
                        @scanner.next()
                        continue
                    else:
                        break
                    }
                }
            }

            if @scanner.current_tok() == Token.Arrow: {
                @scanner.next()
                args[0] = parse_type()
                @scanner.next()
            }

            @scanner.expect(Token.RightParenth)
        }

        var result = TypeSym(raw_class, args)

        return result
    }

    private define parse_parameter: Parameter {
        var keyword = ""

        if @scanner.current_is(Token.Keyarg): {
            keyword = @scanner.current_str()
            @scanner.next()
        }

        var name = @scanner.expect_word()
        var is_optarg = false

        @scanner.require(Token.Colon)
        @scanner.next()

        if @scanner.current_tok() == Token.Multiply: {
            is_optarg = true
            @scanner.next()
        }

        var type = parse_type()
        var value = ""

        if is_optarg: {
            var raw_name = type.base.name

            @scanner.require(Token.Equal)
            type = TypeSym(@optional_cls, [type])
            value = scan_optarg_for(raw_name)
        }

        @scanner.next()

        var p = Parameter(:name name,
                          :keyword keyword,
                          :type type,
                          :value value)

        return p
    }

    private define parse_generics_from(index: Integer): List[String] {
        while 1: {
            var generic_word = @scanner.require(Token.Word)

            if generic_word != @full_generic_names[index]: {
                raise ParseError("Expected generic {0}, but got {1}."
                        .format(@full_generic_names[index],
                                generic_word))
            }

            index += 1
            @scanner.next()

            match @scanner.current_tok(): {
                case Token.Comma:
                    continue
                case Token.RightBracket:
                    break
                else:
                    @scanner.expect_any(Token.Comma, Token.RightBracket)
            }
        }

        @scanner.next()

        return @full_generic_names.slice(0, index)
    }

    public define collect_generics: List[String] {
        var g: List[String] = @visible_generics

        if @scanner.next_is(Token.LeftBracket): {
            g = parse_generics_from(@visible_generics.size())
            @visible_generics = g
        }

        return g
    }

    public define store_generics {
        @saved_generics = @visible_generics
    }

    public define unstore_generics {
        @visible_generics = @saved_generics
    }

    public define clear_generics {
        @visible_generics = []
    }

    public define parse_define_parameters(parameters: List[Parameter]) {
        while 1: {
            var p = parse_parameter()

            parameters.push(p)

            match @scanner.current_tok(): {
                case Token.Comma:
                    @scanner.next()
                case Token.RightParenth:
                    @scanner.next()
                    break
                case Token.ThreeDots:
                    var vararg_type = TypeSym(@vararg_cls, [p.type])

                    p.type = vararg_type

                    @scanner.require(Token.RightParenth)
                    @scanner.next()
                    break
                else:
                    @scanner.expect_any(Token.Comma, Token.RightParenth)
            }
        }
    }

    public define parse_define_result(parameters: List[Parameter]) {
        if @scanner.current_is(Token.Colon): {
            @scanner.require(Token.Word)

            var type = parse_type()

            parameters[0] = Parameter("", "", type, "")
            @scanner.next()
        }
    }

    public define parse_variant_args(new_variant: VariantSym) {
        # Variants aren't Function values, so don't add a Unit at 0.
        @scanner.next()

        while 1: {
            var keyword = ""

            if @scanner.current_is(Token.Keyarg): {
                keyword = @scanner.current_str()
                @scanner.next()
            }

            var type = parse_type()
            var value = ""

            var p = Parameter(:name "",
                              :keyword keyword,
                              :type type,
                              :value "")

            new_variant.add_parameter(p)

            @scanner.next()

            match @scanner.current_tok(): {
                case Token.Comma:
                    continue
                case Token.RightParenth:
                    @scanner.next()
                    break
                else:
                    @scanner.expect_any(Token.Comma, Token.RightParenth)
            }
        }
    }

    # The members of the layout section will later become members of the wrapper
    # struct that bindgen creates. LILY_FOREIGN_HEADER is a macro that will
    # add common fields that foreign objects need.
    private define parse_foreign_layout: List[String] {
        @scanner.next()

        var entry = ""
        var last_token = Token.Invalid
        var fields = ["LILY_FOREIGN_HEADER"]

        # This allows for "empty" foreign values.
        if @scanner.current_is(Token.RightCurly): {
            return fields
        }

        while 1: {
            match @scanner.current_tok(): {
                case Token.Word:
                    if last_token == Token.Word: {
                        entry = entry ++ " " ++ @scanner.current_str()
                    else:
                        entry = entry ++ @scanner.current_str()
                    }
                case Token.LeftBracket:
                    entry = entry ++ "["
                case Token.RightBracket:
                    entry = entry ++ "]"
                case Token.Multiply:
                    if last_token == Token.Word: {
                        entry = entry ++ " *"
                    else:
                        entry = entry ++ "*"
                    }
                case Token.Semicolon:
                    fields.push(entry ++ ";")

                    if @scanner.next_is(Token.RightCurly): {
                        break
                    else:
                        last_token = Token.Semicolon
                        entry = ""
                        continue
                    }

                else:
                    raise ParseError("Unexpected token {0} in layout block."
                    .format(@scanner.current_tok()))
            }

            last_token = @scanner.current_tok()

            @scanner.next()
        }

        return fields
    }

    private define parse_members_for(class_sym: ClassSym) {
        @scanner.next()

        while 1: {
            var word = @scanner.expect_word()
            var scope = DefineScope.Public

            if word == "private": {
                scope = DefineScope.Private
                @scanner.next()
            elif word == "protected":
                scope = DefineScope.Protected
                @scanner.next()
            }

            @scanner.expect_word("var")
            @scanner.require(Token.PropWord)

            var name = @scanner.current_str()

            @scanner.require(Token.Colon)
            @scanner.next()

            var type = parse_type()
            var var_sym = VarSym(name, type)

            var_sym.scope = scope

            class_sym.add_property(var_sym)
            @scanner.next()

            if @scanner.current_is(Token.Comma): {
                @scanner.next()
                continue
            elif @scanner.current_is(Token.RightCurly):
                break
            else:
                @scanner.expect_any(Token.Comma, Token.RightCurly)
            }
        }
    }

    private define cmd_library {
        var name = @scanner.require(Token.Word)
        @scanner.next()

        @root_module = ModuleSym(name)
        @root_module.doc = @pending_doc
    }

    public define parse_constructor {
        var self_p = Parameter("", "", @self_ty, "")
        var parameters = [self_p]

        @scanner.next()

        if @scanner.current_is(Token.RightParenth) == false: {
            parse_define_parameters(parameters)
        else:
            @scanner.next()
        }

        var ctor_sym = DefineSym("<new>", DefineScope.Public, @visible_generics,
                                 parameters, :is_static true, :is_ctor true)

        @target_container.add_define(ctor_sym)
    }

    private define update_self_type(c: ContainerSym, names: List[String]) {
        var types = names.map(|n|
                find_magic_cls(@generic_module, n) ).map(|m| TypeSym(m, []) )

        var new_self = TypeSym(c, types)

        @self_ty = new_self
    }

    private define cmd_foreign {
        @scanner.require_word("class")
        clear_generics()

        var name = get_scope_name()
        var class_sym = ForeignClassSym(name)

        @root_module.add_container(class_sym)
        @target_container = class_sym
        update_self_type(class_sym, [])

        @scanner.next()

        # If there are no parentheses, then omit the constructor entirely.
        # Ordinary native classes get a constructor no matter what.
        if @scanner.current_is(Token.LeftParenth): {
            parse_constructor()
        }

        @scanner.expect(Token.LeftCurly)
        @scanner.require_word("layout")
        @scanner.require(Token.LeftCurly)

        class_sym.layout = parse_foreign_layout()
        class_sym.doc = @pending_doc

        @scanner.require(Token.RightCurly)
        @scanner.next()
    }

    private define cmd_builtin {
        clear_generics()

        if @root_module.name != "builtin": {
            raise ParseError("'builtin' strictly for the builtin package.")
        }

        @scanner.require_word("class")

        var name = get_scope_name()
        var boxed_class = @predefined_module.find_container(name)

        if boxed_class.is_none(): {
            raise ParseError("Builtin class {0} not found.".format(name))
        }

        var raw_class = boxed_class.unwrap()

        # This makes sure the generics are well-formed and the token is updated
        # to point to the end. Don't set the generics because builtin classes
        # already have generics set on them.
        collect_generics()

        @target_container = raw_class
        @root_module.add_container(raw_class)
        update_self_type(raw_class, raw_class.generics)

        raw_class.doc = @pending_doc

        @scanner.next()
    }

    private define cmd_native {
        @scanner.require_word("class")
        clear_generics()

        var name = get_scope_name()
        var generics = collect_generics()
        var class_sym = ClassSym(name, generics)

        class_sym.doc = @pending_doc
        @root_module.add_container(class_sym)
        @target_container = class_sym
        update_self_type(class_sym, generics)

        if @scanner.current_is(Token.LeftParenth) == false &&
           @scanner.current_is(Token.LeftCurly): {
            @scanner.expect_any(Token.LeftParenth, Token.LeftCurly)
        }

        parse_constructor()

        if @scanner.current_is(Token.Less): {
            class_sym.parent = @scanner.require(Token.Word) |> Some
            @scanner.next()
        }

        if @scanner.current_is(Token.LeftCurly): {
            parse_members_for(class_sym)
            @scanner.next()
        }
    }

    private define cmd_enum(is_scoped: *Boolean = false) {
        if is_scoped == true: {
            @scanner.require_word("enum")
        }

        var name = get_scope_name()
        var generics = collect_generics()
        var enum_sym = EnumSym(name, generics, :is_scoped is_scoped)

        @root_module.add_container(enum_sym)
        @target_container = enum_sym
        update_self_type(enum_sym, generics)

        @scanner.expect(Token.LeftCurly)
        @scanner.next()

        while 1: {
            var variant_name = @scanner.expect_word()
            var variant_sym = VariantSym(variant_name)

            enum_sym.add_variant(variant_sym)

            if @scanner.next_is(Token.LeftParenth): {
                parse_variant_args(variant_sym)
            }

            if @scanner.current_is(Token.Comma): {
                @scanner.next()
            elif @scanner.current_is(Token.RightCurly):
                break
            else:
                @scanner.expect_any(Token.Comma, Token.RightCurly)
            }
        }

        @scanner.next()
    }

    private define parse_define_scope(input: String): DefineScope {
        var scope = DefineScope.Toplevel

        if input == "private": {
            scope = DefineScope.Private
            @scanner.next()
        elif input == "protected":
            scope = DefineScope.Protected
            @scanner.next()
        elif @current_scope_name:
            scope = DefineScope.Public
        }

        return scope
    }

    private define parse_static(input: String): Boolean {
        var is_static = false
        if input == "static": {
            @scanner.next()
            is_static = true
        }

        return is_static
    }

    private define cmd_define(qual: *String="") {
        store_generics()

        var scope = parse_define_scope(qual)
        var is_static = parse_static(qual)
        var name = @scanner.require(Token.Word)

        if name == @current_scope_name: {
            @scanner.require(Token.Dot)
            name = @scanner.require(Token.Word)
        elif @current_scope_name:
            # Assume this is for the toplevel and leave the scope.
            leave_scope()
            scope = DefineScope.Toplevel
        }

        var generics = collect_generics()
        var parameters: List[Parameter] = [@unit_param]

        if scope != DefineScope.Toplevel && is_static == false: {
            var p = Parameter("self", "", @self_ty, "")
            parameters.push(p)
        }

        if @scanner.current_is(Token.LeftParenth): {
            @scanner.next()
            if @scanner.current_is(Token.RightParenth): {
                @scanner.expect_any(Token.LeftParenth, Token.LeftCurly)
            }
            parse_define_parameters(parameters)
        }

        parse_define_result(parameters)
        unstore_generics()

        var define_sym = DefineSym(name, scope, generics, parameters,
                                   :is_static is_static,
                                   :is_ctor false)

        define_sym.doc = @pending_doc

        match scope: {
            case DefineScope.Toplevel:
                @root_module.add_define(define_sym)
            else:
                @target_container.add_define(define_sym)
        }
    }

    private define cmd_var {
        if @current_scope_name: {
            leave_scope()
        }

        var name = @scanner.require(Token.Word)

        @scanner.require(Token.Colon)
        @scanner.require(Token.Word)

        var type = parse_type()
        var var_sym = VarSym(name, type)

        var_sym.doc = @pending_doc

        @root_module.add_var(var_sym)
        @scanner.next()
    }

    private define cmd_package_files {
        if @root_module.name != "core": {
            raise ParseError("'PackageFiles' strictly for the core package.")
        }

        @paths = @scanner.strify_to_end()
                         .split("\n")
                         .map(|m| m.strip(" ") )
                         .reject(|s| s == "" )

        # So that this command ends with EOF.
        @scanner.set_source("")
        @scanner.next()
    }

    private define parse_block {
        var command = @scanner.require(Token.Word)

        if command == "define": {
            cmd_define()
        elif command == "foreign":
            cmd_foreign()
        elif command == "native":
            cmd_native()
        elif command == "enum":
            cmd_enum()
        elif command == "protected":
            cmd_define("protected")
        elif command == "private":
            cmd_define("private")
        elif command == "static":
            cmd_define("static")
        elif command == "var":
            cmd_var()
        elif command == "builtin":
            cmd_builtin()
        elif command == "scoped":
            cmd_enum(true)
        elif command == "library":
            cmd_library()
        elif command == "PackageFiles":
            cmd_package_files()
        else:
            raise ParseError("Unknown command '{0}'.".format(command))
        }

        @scanner.expect(Token.EndOfFile)
    }

    private define read_block_from(start: Integer): Boolean
    {
        var empty_line_pos = -1

        for current in start...@read_end_pos: {
            var line = @lines[current]

            if line == "*/\n": {
                var found = false
                var j = -1

                for j in start + 1...@read_end_pos: {
                    line = @lines[j]
                    if line == "\n": {
                        found = true
                        break
                    }
                }

                if found == false: {
                    raise BlockError(start - 1,
                    "Block has no blank dividing line.")
                }

                var command = @lines.slice(start, j + 1).join("")
                var doc = @lines.slice(j, current).join("")
                var line_num = start

                @scanner.set_source(command, line_num)
                @pending_doc = doc
                @read_pos = current + 1

                return true
            }
        }

        raise BlockError(start - 1, "Block is unterminated.")
    }

    public define read_next: Boolean {
        var i = @read_pos
        var end = @read_end_pos

        while i < end: {
            if @lines[i] == "/**\n": {
                read_block_from(i + 1)
                return true
            }

            i += 1
        }

        return false
    }

    private define read_blocks: Result[Tuple[String, Integer], ModuleSym] {
        try: {
            while read_next(): {
                parse_block()
            }
        except ParseError as e:
            return Failure(<[e.message, @scanner.line_num]>)
        except BlockError as e:
            return Failure(<[e.message, e.line]>)
        }

        return Success(@root_module)
    }

    public define process_lines(to_read: List[String])
        : Result[Tuple[String, Integer], ModuleSym]
    {
        @lines = to_read
        @read_end_pos = to_read.size() - 1

        return read_blocks()
    }

    public define process_file(filename: String)
        : Result[Tuple[String, Integer], ModuleSym]
    {
        var local_lines: List[String] = []

        var f = File.open(filename, "r")
        f.each_line(|l| l.encode().unwrap() |> local_lines.push )
        f.close()

        @lines = local_lines
        @read_end_pos = local_lines.size() - 1

        var result = read_blocks()

        if result.is_success() && @root_module.name == "core": {
            var split_base_dir = filename.split("/")

            split_base_dir[-1] = ""

            var base_dir = split_base_dir.join("/")
            var root = result.success().unwrap()
            var modules: List[ModuleSym] = []

            for i in 0...@paths.size() - 1: {
                var driver = CSourceDriver()

                driver.process_file(base_dir ++ @paths[i])
                      .success()
                      .unwrap()
                      |> modules.push
            }

            @root_module.modules = modules
        }

        return result
    }
}
